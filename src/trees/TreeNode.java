package trees;

/**
 * Created by tomor on 06/05/2019.
 */
public class TreeNode {
    private int data;
    private TreeNode left;
    private TreeNode right;

    public TreeNode(int data){
        this.data = data;
    }
    public TreeNode(int data, TreeNode left, TreeNode right){
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public int size(){
        int size = 1;

        if (this.left != null){
            size += this.left.size();
        }

        if (this.right != null){
            size += this.right.size();
        }

        return size;
    }

    public int sum(){
        int sum = this.data;

        if (this.left != null){
            sum += this.left.sum();
        }

        if (this.right != null){
            sum += this.right.sum();
        }

        return sum;
    }
    public int max(){
        if (this.right != null){
            return this.right.max();
        }

        return this.data;
    }

    // leftmost()
    public int min(){
        if (this.left != null){
            return this.left.min();
        }

        return this.data;
    }

    public boolean exists(int value){
        if (value == data){
            return true;
        }else{
            if (value < data){
                if (left == null){
                    return false;
                }else{
                    return left.exists(value);
                }
            }else{
                if (right == null){
                    return false;
                }else{
                    return right.exists(value);
                }
            }
        }
    }
    public void insert(int value){
        if (value <= data){
            if (left == null){
                left = new TreeNode(value);
            }else{
                left.insert(value);
            }
        }else if (value > data){
            if (right == null){
                right = new TreeNode(value);
            }else{
                right.insert(value);
            }
        }
    }
    public void printPreOrder(){
        System.out.print(data + " ");

        if (this.left != null){
            this.left.printPreOrder();
        }

        if (this.right != null){
            this.right.printPreOrder();
        }
    }

    public void printInOrder(){
        if (this.left != null){
            this.left.printInOrder();
        }
        System.out.print(data + " ");
        if (this.right != null){
            this.right.printInOrder();
        }
    }
    public void printPostOrder(){
        if (this.left != null){
            this.left.printPostOrder();
        }
        if (this.right != null){
            this.right.printPostOrder();
        }
        System.out.print(data + " ");
    }

    public boolean isBST(){
        boolean leftBst = true;
        boolean rightBst = true;

        if (this.left != null){
            if (data < this.left.data){
                return false;
            }

            leftBst = this.left.isBST();

            if (!leftBst) return false;
        }

        if (this.right != null){
            if (data > this.right.data){
                return false;
            }

            rightBst = this.right.isBST();
        }

        return leftBst && rightBst;
    }
    private boolean isLeafNode(){
        return this.left == null && this.right == null;
    }
    public TreeNode remove(int item){
        if (this.data == item){
            if (isLeafNode()){
                return null;
            }else if (this.left == null){
                return this.right;
            }else if (this.right == null){
                return this.left;
            }else{
                this.data = this.right.min();
                this.right = this.right.remove(this.data);
            }
        }else{
            if (this.left != null){
                this.left = this.left.remove(item);
            }

            if (this.right != null){
                this.right = this.right.remove(item);
            }
        }

        return this;
    }
    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }
}
