package trees;

/**
 * Created by tomor on 06/05/2019.
 */
public class BinarySearchTree {
    private TreeNode root;

    public boolean isEmpty(){
        return root == null;
    }

    public void insert(int value){
        if (root == null){
            root = new TreeNode(value);
        }else{
            root.insert(value);
        }
    }
    public int size(){
        return root == null ? 0 : root.size();
    }
    public int max(){
        if (root == null){
            return 0;
        }

        return root.max();
    }
    public int sum(){
        return this.root != null ? this.root.sum() : 0;
    }
    public boolean exists(int value){
        return this.root.exists(value);
    }

    public void printPreOrder(){
        root.printPreOrder();
    }

    public void printInOrder(){
        root.printInOrder();
    }

    public boolean isBST(){
        return this.root.isBST();
    }

    public void remove(int item){
        this.root.remove(item);
    }
}
