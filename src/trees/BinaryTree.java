package trees;

/**
 * Created by tomor on 08/05/2019.
 */
public class BinaryTree {
    private TreeNode root;

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public boolean isBST(){
        if (root == null){
            // throw exception
        }
        return root.isBST();
    }
}
