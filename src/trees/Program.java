package trees;

/**
 * Created by tomor on 06/05/2019.
 */
public class Program {
    public static void main(String[] args){
        BinarySearchTree bst = new BinarySearchTree();
//        bst.insert(43);
//        bst.insert(31);
//        bst.insert(64);
//        bst.insert(20);
//        bst.insert(40);
//        bst.insert(28);
//        bst.insert(33);
//        bst.insert(56);
//        bst.insert(89);
//        bst.insert(47);
//        bst.insert(59);

        bst.insert(50);
        bst.insert(65);
        bst.insert(34);
        bst.insert(12);
        bst.insert(16);
        bst.insert(51);
        bst.insert(68);
        bst.insert(69);


        bst.printPreOrder();
        System.out.println();
        bst.remove(50);
        bst.printPreOrder();

    }
}
