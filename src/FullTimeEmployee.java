/**
 * Created by tomor on 06/03/2019.
 */
public class FullTimeEmployee extends Employee {
    private double annualSalary;

    public FullTimeEmployee(String number, String name, double annualSalary){
        super(number, name);

        this.annualSalary = annualSalary;
    }

    public void setAnnualSalary(double annualSalary){
        if (annualSalary > 0){
            this.annualSalary = annualSalary;
        }
    }

    public double getAnnualSalary(){
        return this.annualSalary;
    }

    public double calculateMonthlyPay(){
        return this.annualSalary / 12;
    }

    public String getStatus(){
        return "FULL-TIME";
    }

}
