package stacks;

/**
 * Created by tomor on 25/03/2019.
 */
public interface Stack<T> {
    public void push(T element);
    public T pop();
    public T peek();
    public boolean isEmpty();
    public void clear();
}
