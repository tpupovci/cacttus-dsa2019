package stacks;

import lists.ArrayBasedList;

/**
 * Created by tomor on 25/03/2019.
 */
public class ListBasedStack<T> extends AbstractStack<T> {
    private ArrayBasedList<T> items;
    private int topIndex = -1;

    public ListBasedStack(){
        items = new ArrayBasedList<T>();
    }


    @Override
    public void push(T element) {
        items.add(element);
        topIndex++;
    }

    @Override
    public T pop() {
        checkEmpty();
        T value = items.get(topIndex);
        items.remove(topIndex--);

        return value;
    }

    @Override
    public T peek() {
        checkEmpty();
        return items.get(topIndex);
    }

    @Override
    public boolean isEmpty() {
        return topIndex == -1;
    }

    @Override
    public void clear() {
        items = new ArrayBasedList<T>();
        topIndex = -1;
    }

    public String toString(){
        String res = "";

        for (int i = topIndex; i >= 0; i--){
            res += items.get(i) + "\n";
        }
        return res;
    }
}
