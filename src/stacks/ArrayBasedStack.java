package stacks;

/**
 * Created by tomor on 25/03/2019.
 */
public class ArrayBasedStack<T> extends AbstractStack<T>{
    private T items[];
    private int topIndex;
    private static final int MAX_ELEMENTS = 50;

    public ArrayBasedStack(){
        items = (T[]) new Object[MAX_ELEMENTS];
        topIndex = -1;
    }
    @Override
    public void push(T element) {
        topIndex++;
        items[topIndex] = element;
    }

    @Override
    public T pop() {
        checkEmpty();
        return items[topIndex--];
    }

    @Override
    public T peek() {
        checkEmpty();

        return items[topIndex];
    }

    @Override
    public boolean isEmpty() {
        return topIndex == -1;
    }

    @Override
    public void clear() {
        items = (T[]) new Object[MAX_ELEMENTS];
        topIndex = -1;
    }
}
