package stacks;

import lists.LinkedList;

/**
 * Created by tomor on 25/03/2019.
 */
public class LinkedListBasedStack<T> extends AbstractStack<T>{
    private LinkedList<T> items;

    public LinkedListBasedStack(){
        items = new LinkedList<T>();
    }
    @Override
    public void push(T element) {
        items.add(0, element);
    }

    @Override
    public T pop() {
        checkEmpty();

        T value = items.get(0);
        items.remove(0);

        return value;
    }

    @Override
    public T peek() {
        checkEmpty();

        return items.get(0);
    }

    @Override
    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public void clear() {
        items.clear();
    }
}
