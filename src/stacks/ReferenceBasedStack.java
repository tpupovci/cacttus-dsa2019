package stacks;

/**
 * Created by tomor on 25/03/2019.
 */
public class ReferenceBasedStack<T> extends AbstractStack<T>{
    private Node<T> top;

    @Override
    public void push(T element) {
        if (top == null){
            top = new Node(element);
        }else{
            top = new Node(element, top);
        }
    }

    @Override
    public T pop() {
        checkEmpty();

        Node<T> currentTop = top;
        top = top.next;


        return currentTop.value;
    }

    @Override
    public T peek() {
        checkEmpty();

        return top.value;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

    @Override
    public void clear() {
        top = null;
    }

    private class Node<T> {
        private T value;
        private Node next;

        public Node(T value, Node next){
            this.value=value;
            this.next = next;
        }

        public Node(T value){
            this.value = value;
        }
    }
}
