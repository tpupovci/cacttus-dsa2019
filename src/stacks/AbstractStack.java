package stacks;

/**
 * Created by tomor on 25/03/2019.
 */
public abstract class AbstractStack<T> implements Stack<T>{

    protected void checkEmpty(){
        if (isEmpty()){
            throw new StackEmptyException();
        }
    }
}
