package stacks;

import java.util.Scanner;

/**
 * Created by tomor on 25/03/2019.
 */
public class Program {
    public static void main(String[] args){

        String[] exprs = new String[] { "{ab}}c",
                "{a{b}c}", "{{ab}", "{(abc)}",
                "{(abc}", "{{(abc}}}", "({ab)}"};

        for(String expr : exprs){
            System.out.printf("%s balanced?: %s\n", expr, isBalanced(expr));
        }

        System.out.println(convertToPostfix("1 + ( 6 - 2 ) * 2")); // Rezultati: 1 6 2 - 2 * +
        System.out.println(convertToPostfix("1 + 6 - 2 * 2")); // Rezultati: 1 6 2 - 2 * +
        System.out.println(convertToPostfix("1 + 6 - 2 * 2 + 5")); // Rezultati: 1 6 + 2 2 * - 5 + = 8
//        System.out.println(evaluatePostfix(convertToPostfix("1 + 6 - 2 * 2 + 5"))); // Rezultati: 1 6 + 2 2 * - 5 + = 8
//
//        System.out.println(evaluatePostfix("1 6 2 - 2 * +")); // Rezultati: 9
//        System.out.println(evaluatePostfix("10 6 2 - 2 * +")); // Rezultati: 18

    }

    public static String convertToPostfix(String infixExpression){
        Stack<String> st = new ListBasedStack<String>();
        String postfixExpression = "";
        Scanner sc = new Scanner(infixExpression);

        while(sc.hasNext()){
            if (sc.hasNextInt()){
                postfixExpression += sc.next() + " ";
            }else{
                String character = sc.next();
                switch(character){
                    case "+":case "-":case "*":case "/":
                        while (!st.isEmpty() && precedence(character.charAt(0)) <= precedence(st.peek().charAt(0)) ){
                            postfixExpression += st.pop() + " ";
                        }

                        st.push(character);
                        break;
                    case "(":
                        st.push(character);

                        break;
                    case ")":
                        String topOperator = st.pop();
                        while (!st.isEmpty() && !topOperator.equals("(")){
                            postfixExpression += topOperator + " ";
                            topOperator = st.pop();
                        }
                        break;
                }
            }
        }

        while (!st.isEmpty()){
            postfixExpression += st.pop() + " ";
        }

        return postfixExpression;
    }

    public static int precedence(char op){
        if (op == '(' || op == ')')
            return 1;
        else if (op == '*' || op == '/')
            return 3;
        else if (op == '+' || op == '-')
            return 2;

        return 0;
    }
    public static int evaluatePostfix(String expression){
        Stack<Integer> st = new ListBasedStack<Integer>();
        Scanner sc = new Scanner(expression);

        while(sc.hasNext()){
            if (sc.hasNextInt()){
                st.push(sc.nextInt());
            }else{
                String character = sc.next();

                int num2 = st.pop();
                int num1 = st.pop();

                switch(character){
                    case "+":
                        st.push(num1+num2);
                        break;
                    case "-":
                        st.push(num1-num2);
                        break;
                    case "*":
                        st.push(num1*num2);
                        break;
                    case "/":
                        st.push(num1/num2);
                        break;
                }
            }
        }

//        for(int i = 0; i < expression.length(); i++){
//            char character = expression.charAt(i);
//
//            if (Character.isDigit(character)){
//                st.push(Integer.parseInt(character + ""));
//            }else{
//                if (character == '+' || character == '-' || character == '*'
//                        || character == '/'){
//                    int num2 = st.pop();
//                    int num1 = st.pop();
//
//                    switch(character){
//                        case '+':
//                            st.push(num1+num2);
//                            break;
//                        case '-':
//                            st.push(num1-num2);
//                            break;
//                        case '*':
//                            st.push(num1*num2);
//                            break;
//                        case '/':
//                            st.push(num1/num2);
//                            break;
//                    }
//                }
//            }
//        }
        return st.pop();
    }

    public static boolean isNumber(char ch){
        String character = ch + "";

        try {
            Integer.parseInt(character);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    public static boolean isBalanced(String expression){
        Stack<Character> st = new ReferenceBasedStack<Character>();

        for (int i = 0; i < expression.length(); i++){
            if (expression.charAt(i) == '{' || expression.charAt(i) == '('){
                st.push(expression.charAt(i));
            }else if (expression.charAt(i) == '}' || expression.charAt(i) == ')'){
                if (st.isEmpty() || !isPaired(expression.charAt(i), st.peek())){
                    return false;
                }

                st.pop();
            }
        }
        return st.isEmpty();
    }

    public static boolean isPaired(char b1, char b2){
        return (b1 == '{' && b2 == '}') || (b1 == '}' && b2 == '{') ||
                (b1 == '(' && b2 == ')') || (b1 == ')' && b2 == '(');
    }

}
