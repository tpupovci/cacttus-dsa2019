package lists;

/**
 * Created by tomor on 13/03/2019.
 */
public class Person {
    private String personalNumber;
    private String firstName;
    private String lastName;

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean equals(Object o){
        Person otherPerson = (Person) o;

        if (otherPerson.getPersonalNumber() == this.getPersonalNumber()
                && otherPerson.getFirstName() == this.getFirstName()
                && otherPerson.getLastName() == this.getLastName()){
            return true;
        }else{
            return false;
        }

    }
}
