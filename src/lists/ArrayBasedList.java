package lists;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by tomor on 11/03/2019.
 */
public class ArrayBasedList<T> extends AbstractList<T> {
    private T[] values;
    private static int DEFAULT_CAPACITY = 10;
    private ArrayIterator iterator = new ArrayIterator();

    public ArrayBasedList(){
        this(DEFAULT_CAPACITY);
    }

    public ArrayBasedList(int capacity){
        values = (T[]) new Object[capacity];
        size = 0;
    }

    public void add(int index, T value)  {
        ensureCorrectIndex(index);
        checkCapacity();

        for (int i = size; i > index; i--){
            values[i] = values[i - 1];
        }

        values[index] = value;
        size++;
    }

    public void remove(int index)  {

        ensureCorrectIndex(index);

        for(int i = index; i < size; i++){
            values[i] = values[i + 1];
        }

        size--;

       // values[size] = 0;
    }

    public void set(int index, T value) {
        throw new NotImplementedException();
    }

    // postcondition: the internal list contains no elements
    // and size is equal to 0

    public void removeAll(){
        this.values = (T[]) new Object[DEFAULT_CAPACITY];
        this.size = 0;
    }
    public T get(int index) throws IndexNotFoundException {
        ensureCorrectIndex(index);

        return values[index];
    }

    public int indexOf(T value){
        for(int i = 0; i < size; i++){
            if (values[i].equals(value)){
                return i;
            }
        }

        return -1;
    }

    // postcondition: returns true if all elements of list elements are in
    // this list, otherwise false

    public boolean containsAll(ArrayBasedList<T> elements){
        for(int i = 0; i < elements.size(); i++){
            try {
                if (!this.contains(elements.get(i))) {
                    return false;
                }
            } catch (IndexNotFoundException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean containsAny(ArrayBasedList<T> elements){
        for(int i = 0; i < elements.size(); i++){
            try {
                if (this.contains(elements.get(i))) {
                    return true;
                }
            } catch (IndexNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean equals(Object o){
        ArrayBasedList<T> other = (ArrayBasedList<T>) o;

        if (this.size() != other.size()){
            return false;
        }

        for(int i = 0; i < size; i++){
            try {
                if (!this.get(i).equals(other.get(i))){
                    return false;
                }
            } catch (IndexNotFoundException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    public String toString(){
        String arrStr = "[";

        if (size > 0){
            for (int i = 0; i < size - 1; i++){
                arrStr += values[i] + ", ";
            }

            arrStr += values[size - 1];
        }

        arrStr += "]";

        return arrStr;
    }

    private void ensureCorrectIndex(int index) throws IndexNotFoundException {
        if (index < 0 || index > size) {
            throw new IndexNotFoundException();
        }
    }
    private void checkCapacity(){
        if (size == values.length){
            T[] tempValues = (T[]) new Object[values.length*2];
            for (int i = 0; i < values.length; i++){
                tempValues[i] = values[i];
            }

            values = tempValues;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return this.iterator;
    }

    @Override
    public void forEach(Consumer<? super T> action) {

    }

    @Override
    public Spliterator<T> spliterator() {
        return null;
    }

    private class ArrayIterator implements Iterator<T> {

        private int index;   // current position in list

        public ArrayIterator() {
            index = 0;
        }

        public boolean hasNext() {
            return index < size();
        }

        public T next() {
            index++;
            return get(index - 1);
        }

        public void remove() {
            ArrayBasedList.this.remove(index - 1);
            index--;
        }

        public void forEachRemaining(Consumer<? super T> action) {

        }
    }
}
