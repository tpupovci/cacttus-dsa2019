package lists;

import java.io.*;

public class Program {
    public static void main(String[] args) throws FileNotFoundException, IndexNotFoundException {
        List<Integer> list1 = new ArrayBasedList<Integer>();
        processList(list1);

        List<Integer> list2 = new LinkedList<Integer>();
        processList(list2);

        for (Integer i : list2){
            System.out.println(i);
        }

    }

    public static void processList(List<Integer> list){
        list.add(18);
        list.add(27);
        list.add(93);
        System.out.println(list);
        list.remove(1);
        System.out.println(list);
    }
}
