package lists;

/**
 * Created by tomor on 20/03/2019.
 */
public abstract class AbstractList<T> implements List<T> {
    protected int size;

    public void add(T value){
        add(size(), value);
    }
    public boolean contains(T value){
        return indexOf(value) != -1;
    }
    public boolean isEmpty(){
        return size() == 0;
    }
    public int size(){
        return size;
    }
}
