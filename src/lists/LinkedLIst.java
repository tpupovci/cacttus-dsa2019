package lists;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by tomor on 18/03/2019.
 */
public class LinkedList<T> extends AbstractList<T> {
    private ListNode<T> front;
    private LinkedListIterator iterator = new LinkedListIterator();

    public void add(int index, T value){
        if (index == 0){
            front = new ListNode<T>(value, front);
        }else{
            ListNode<T> current = front;

            for (int i = 0; i < index - 1; i++){
                current = current.next;
            }

            current.next = new ListNode<T>(value, current.next);

            // [0,1,2,3,4]


        }

        size++;
    }

    public T get(int index){
        ListNode<T> current = front;

        for (int i = 0; i < index; i++){
            current = current.next;
        }

        return current.data;
    }


    public void clear(){
        front = null;
        size = 0;
    }

    public int indexOf(T value){
        for (int i = 0; i < size; i++){
            if (get(i) == value){
                return i;
            }
        }

        return -1;
    }
    public void remove(int index){
        if (index == 0)
            remove();
        else{
            ListNode<T> current = front;
            for (int i = 0; i < index - 1; i++){
                current = current.next;
            }

            current.next = current.next.next;
        }

        size--;
    }

    @Override
    public void set(int index, T value) {
        throw new NotImplementedException();
    }

    public T remove(){
        if (front == null)
            throw new NoSuchElementException();

        ListNode<T> current = front;

        front = front.next;

        size--;

        return current.data;
    }

    public String toString(){
        return printList(front);
    }

    private String printList(ListNode<T> node){
        if (node == null){
            return "";
        }else{
            return node.data + " " + printList(node.next);
        }
    }

    public int sizeRecursive(){
        return sizeRecursive(front);
    }

    private int sizeRecursive(ListNode<T> node){
        if (node == null){
            return 0;
        }else{
            return 1 + sizeRecursive(node.next);
        }
    }
    @Override
    public Iterator<T> iterator() {
        return this.iterator;
    }

    @Override
    public void forEach(Consumer<? super T> action) {

    }

    @Override
    public Spliterator<T> spliterator() {
        return null;
    }

    private class ListNode<T> {
        public T data;
        public ListNode next;

        public ListNode(T data){
            this.data = data;
        }

        public ListNode(T data, ListNode next){
            this.data = data;
            this.next = next;
        }

        public String toString(){
            return ""+data;
        }
    }

    private class LinkedListIterator implements Iterator<T> {
        private ListNode<T> current;   // current position in list

        public LinkedListIterator() {
            current = front;
        }

        public boolean hasNext() {
            return current != null;
        }

        public T next() {
            T result = current.data;
            current = current.next;
            return result;
        }

        public void remove() {      // not implemented for now
            throw new UnsupportedOperationException();
        }

    }
}
