package lists;

/**
 * Created by tomor on 20/03/2019.
 */
public interface List<T> extends Iterable<T> {
    public void add(T value);
    public void add(int index, T value);
    public T get(int index);
    public int indexOf(T value);
    public boolean isEmpty();
    public void remove(int index);
    public void set(int index, T value);
    public int size();
}
