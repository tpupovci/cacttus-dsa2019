/**
 * Created by tomor on 06/03/2019.
 */
public class Employee {
    private String number;
    private String name;

    public Employee(String number, String name){
        this.number = number;
        this.name = name;
    }

    public String getNumber(){
        return this.number;
    }

    public String getName(){
        return this.name;
    }
}
