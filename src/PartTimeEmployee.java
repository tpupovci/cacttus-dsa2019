/**
 * Created by tomor on 06/03/2019.
 */
public class PartTimeEmployee extends Employee {
    private double hourlyPay;

    public PartTimeEmployee(String number, String name, double hourlyPay){
        super(number, name);

        this.hourlyPay = hourlyPay;
    }

    public void setHourlyPay(double hourlyPay){
        this.hourlyPay = hourlyPay;
    }

    public double getHourlyPay(){
        return this.hourlyPay;
    }

    public double calculateWeeklyPay(int numberOfHours){
        return numberOfHours * hourlyPay;
    }

    public String getStatus(){
        return "PART-TIME";
    }
}
