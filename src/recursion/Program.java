package recursion;

import lists.LinkedList;

import java.util.Arrays;

/**
 * Created by tomor on 10/04/2019.
 */
public class Program {
    public static void main(String[] args){

        int[] numbers = new int[100];

        for (int i = 0; i < 100; i++){
            numbers[i] = i + 1;
        }

        System.out.println(binarySearch(99, numbers, 0, numbers.length-1));

        System.out.println(Math.pow(2, -5));
        System.out.println(powRecursive(2,-5));
        System.out.println(powRecursive(2,5));

        System.out.println(decimalToBinary(29));

        LinkedList<Integer> nrs = new LinkedList<Integer>();
        nrs.add(1);
        nrs.add(5);
        nrs.add(80);

        System.out.println(nrs);
        System.out.println(nrs.sizeRecursive());
    }

    public static String decimalToBinary(int n){
        if (n > 0){
            return decimalToBinary(n/2) + n % 2;
        }

        return "";
    }
    public static int pow(int base, int exp){
        int result = 1;

        for (int i = 0; i < exp; i++){
            result *= base;
        }

        return result;
    }
    public static double powRecursive(int base, int exp){
        if (exp == 0){
            return 1;
        }else{
            return exp < 0 ? 1.0/base * powRecursive(base, exp + 1) : base*powRecursive(base, exp - 1);
        }
    }
    public static int binarySearch(int value, int[] numbers, int start, int end){
        if ((end-start) == 1){
            if (numbers[end] == value)
                return end;
            else
                return -1;
        }else{
            int mid = (start + end) / 2;

            if (value < numbers[mid - 1]){
                return binarySearch(value, numbers, start, mid - 1);
            }else if (value > numbers[mid]){
                return binarySearch(value, numbers, mid, end);
            }else {
                return mid;
            }
        }
    }
}
