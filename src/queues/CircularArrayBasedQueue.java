package queues;

import java.util.Arrays;

/**
 * Created by tomor on 01/04/2019.
 */
public class CircularArrayBasedQueue<T> implements Queue<T> {

    private T[] elements;
    private int front;
    private int rear;
    private int size;

    public CircularArrayBasedQueue(){
        this(10);
    }

    public CircularArrayBasedQueue(int initialCapacity){
        elements = (T[]) new Object[initialCapacity];
    }

    @Override
    public void enqueue(T element) {
        if (isFull()){
            throw new QueueFullException();
        }

        elements[rear] = element;

        rear = (rear + 1) % elements.length;

        size++;

    }

    @Override
    public T dequeue() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        T value = elements[front];

        front = (front + 1) % elements.length;
        size--;

        return value;
    }

    @Override
    public T peek() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        return elements[front];
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean isFull() {
        return size() == elements.length;
    }
    @Override
    public int size() {
        return size;
    }

    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("Queue: ");
        for (int i = 0; i < size; i++){
            buffer.append(elements[(front + i)%elements.length]);
            buffer.append(" ");
        }

        return buffer.toString();
    }
}
