package queues;

/**
 * Created by tomor on 01/04/2019.
 */
public class ArrayBasedQueue<T> implements Queue<T> {
    private T[] elements;
    private int count;

    public ArrayBasedQueue(){
        this(10);
    }

    public ArrayBasedQueue(int initialCapacity){
        elements = (T[]) new Object[initialCapacity];
    }
    @Override
    public void enqueue(T element) {
        if (isFull()){
            throw new QueueFullException();
        }
        elements[count++] = element;
    }

    @Override
    public T dequeue() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        T value = elements[0];
        for (int i = 0; i < count; i++){
            elements[i] = elements[i+1];
        }
        elements[count--] = null;

        return value;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new QueueEmptyException();
        }
        return elements[0];
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    public boolean isFull(){
        return count == elements.length;
    }
    @Override
    public int size() {
        return count;
    }
}
