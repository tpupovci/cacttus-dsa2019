package queues;

import stacks.ReferenceBasedStack;
import stacks.Stack;

/**
 * Created by tomor on 01/04/2019.
 */
public class Program {

    public static void main(String[] args){
        Queue<Integer> q = new ReferenceBasedQueue<Integer>();

        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.enqueue(300);

        reverse(q);
        System.out.println(q);
        System.out.println(sum(q));

        System.out.println(q);
    }

    public static Queue<Integer> reverse(Queue<Integer> queue){
        Stack<Integer> st = new ReferenceBasedStack<Integer>();

        while(!queue.isEmpty()){
            st.push(queue.dequeue());
        }

        while(!st.isEmpty()){
            queue.enqueue(st.pop());
        }

        return queue;
    }

    public static int sum(Queue<Integer> queue){
        int sum = 0;
        int size = queue.size();

        for(int i = 0; i < size; i++){
            System.out.println(queue);
            int n = queue.dequeue();

            queue.enqueue(n);

            sum += n;
        }

        return sum;
    }
}
