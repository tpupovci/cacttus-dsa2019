package queues;

/**
 * Created by tomor on 01/04/2019.
 */
public class ReferenceBasedQueue<T> implements Queue<T>{
    private Node<T> front;
    private Node<T> rear;
    private int size;

    @Override
    public void enqueue(T element) {
        if (isEmpty()){
            front = rear = new Node(element, null);
        }else{
            rear.next = new Node(element, null);

            rear = rear.next;
        }

        size++;
    }

    @Override
    public T dequeue() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        T value = front.value;

        front = front.next;
        size--;

        if (isEmpty()){
            rear = null;
        }
        return value;
    }

    @Override
    public T peek() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        return front.value;
    }

    public String toString(){
        Node<T> current = front;
        StringBuffer b = new StringBuffer();
        b.append("[");

        while(current != null){
            b.append(current.value);
            b.append(",");

            current = current.next;
        }

        b.append("]");

        return b.toString();
    }
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return size;
    }


    private class Node<T> {
        T value;
        Node next;

        public Node(T value, Node next){
            this.value = value;
            this.next = next;
        }
    }
}
