package queues;

/**
 * Created by tomor on 03/04/2019.
 */
public class PriorityQueue<T extends Comparable<T>> implements Queue<T> {
    private Node<T> front;
    private Node<T> rear;
    private int size;

    public void enqueue(T element) {
        Node current = front;

        if (isEmpty()) {
            front = rear = new Node(element, null);

        } else if (((Comparable<T>)current.value).compareTo(element) < 0){
            front = new Node(element, front);
        }else{
            while(current.next != null && ((Comparable<T>)current.next.value).compareTo(element) > 0){
                current = current.next;
            }

            current.next = new Node(element, current.next);

            rear = current.next;
        }

        size++;
    }
    public T dequeue() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        T value = front.value;

        front = front.next;
        size--;

        if (isEmpty()){
            rear = null;
        }
        return value;
    }

    public T peek() {
        if (isEmpty()){
            throw new QueueEmptyException();
        }

        return front.value;
    }

    public String toString(){
        Node current = front;
        StringBuffer b = new StringBuffer();
        b.append("[");

        while(current != null){
            b.append(current.value);
            b.append(",");

            current = current.next;
        }

        b.append("]");

        return b.toString();
    }
    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }


    private class Node<T> {
        T value;
        Node next;

        public Node(T value, Node next){
            this.value = value;
            this.next = next;
        }
    }
}
