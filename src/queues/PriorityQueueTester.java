package queues;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tomor on 03/04/2019.
 */
public class PriorityQueueTester {

    public static void main(String[] args){
        // Queue<Issue> queue = new ReferenceBasedQueue<Issue>();

        Queue<Issue> queue = new PriorityQueue<Issue>();

        // []
        // [LOW]
        // [HIGH, LOW]
        // [HIGH, MEDIUM, MEDIUM, LOW]

        // [HIGH, HIGH, MEDIUM, LOW]
        // [HIGH, HIGH, MEDIUM, MEDIUM, LOW]
        // [HIGH, HIGH, MEDIUM, MEDIUM, LOW, LOW]
        queue.enqueue(new Issue("user2123", "I can't access my email", Issue.IssuePriority.MEDIUM));
        queue.enqueue(new Issue("user023", "I am not able to log into my computer", Issue.IssuePriority.HIGH));
        queue.enqueue(new Issue("user1111", "My printer is not working", Issue.IssuePriority.LOW));
        queue.enqueue(new Issue("user023", "I am not able to log into my computer", Issue.IssuePriority.HIGH));
        queue.enqueue(new Issue("user2123", "I can't access my email", Issue.IssuePriority.MEDIUM));
        queue.enqueue(new Issue("user1111", "My printer is not working", Issue.IssuePriority.LOW));

        System.out.println(queue);

        Queue<Integer> q2 = new PriorityQueue<Integer>();
        q2.enqueue(1);
        q2.enqueue(10);
        q2.enqueue(300);
        q2.enqueue(2);
        q2.enqueue(3);

        System.out.println("1,10,300,2,3");
        System.out.println(q2);

        // Comparable
    }
}
