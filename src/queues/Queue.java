package queues;

/**
 * Created by tomor on 01/04/2019.
 */
public interface Queue<T> {
    public void enqueue(T element);
    public T dequeue();
    public T peek();
    public boolean isEmpty();
    public int size();
}
