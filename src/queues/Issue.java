package queues;

/**
 * Created by tomor on 03/04/2019.
 */
public class Issue implements Comparable<Issue> {
    private String submittedBy;
    private String description;
    private IssuePriority priority;

    public Issue(String submittedBy, String description, IssuePriority priority){
        this.submittedBy = submittedBy;
        this.description = description;
        this.priority = priority;
    }

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IssuePriority getPriority() {
        return priority;
    }

    public void setPriority(IssuePriority priority) {
        this.priority = priority;
    }

    public String toString(){
        return this.getPriority().toString();
    }

    @Override
    public int compareTo(Issue o) {
        if (this.getPriorityValue() < o.getPriorityValue()){
            return -1;
        }else if (this.getPriorityValue() > o.getPriorityValue()){
            return 1;
        }else {
            return 0;
        }
    }

    private int getPriorityValue(){
        switch(priority){
            case LOW:
                return 1;
            case MEDIUM:
                return 2;
            case HIGH:
                return 3;
        }

        return 0;
    }

    protected enum IssuePriority {
        LOW, MEDIUM, HIGH
    }
}
