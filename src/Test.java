import java.util.Arrays;

/**
 * Created by tomor on 06/03/2019.
 */
public class Test {
    public static void main(String[] args){
        int[] arr = new int[] {10,9,8,7,6,5,4,3,2,1};
        mergeSort(arr);
    }

    public static void bubbleSort(int[] a){
        int count = 0;

        for (int i = 0; i < a.length; i++){

            for (int j = 1; j < a.length - i; j++){

                if (a[j - 1] > a[j]){
                    swap(a, j - 1, j);
                    count++;
                }
                System.out.println(Arrays.toString(a));
            }
        }

        System.out.println(count);
    }
    public static void mergeSort(int[] a) {
        int[] temp = new int[a.length];
        System.out.println(Arrays.toString(a));

        mergeSort(a, temp, 0, a.length - 1);
    }

    private static void mergeSort(int[] a, int[] temp,
                                  int left, int right) {
        if (left >= right) {  // base case
            return;
        }

        // sort the two halves
        int mid = (left + right) / 2;
        mergeSort(a, temp, left, mid);
        mergeSort(a, temp, mid + 1, right);

        // merge the sorted halves into a sorted whole
        merge(a, temp, left, right);
    }

    private static void merge(int[] a, int[] temp,
                              int left, int right) {
        int mid = (left + right) / 2;
        int count = right - left + 1;

        int l = left;                  // counter indexes for L, R
        int r = mid + 1;

        // main loop to copy the halves into the temp array
        for (int i = 0; i < count; i++)
            if (r > right) {           // finished right; use left
                temp[i] = a[l++];
            } else if (l > mid) {      // finished left; use right
                temp[i] = a[r++];
            } else if (a[l] < a[r]) {  // left is smaller (better)
                temp[i] = a[l++];
            } else {                   // right is smaller (better)
                temp[i] = a[r++];
            }

        // copy sorted temp array back into main array
        for (int i = 0; i < count; i++) {
            a[left + i] = temp[i];
        }
        System.out.println(Arrays.toString(Arrays.copyOfRange(a, left, right)));
    }


    public static void selectionSort(int[] a){
        int count = 0;
        for (int i = 0; i < a.length; i++){
            int minIndex = i;

            for (int j = i + 1; j < a.length; j++){
                count++;
                if (a[j] < a[minIndex])
                    minIndex = j;
            }

            swap(a, i, minIndex);
        }

        System.out.println(count);
    }
    public static void insertionSort(int[] a){
        int count = 0;
        for (int i = 1; i < a.length; i++){
            int temp = a[i];

            int j = i;

            for (j = i; j > 0 && a[j - 1] > temp; j--){
                count++;
                a[j] = a[j - 1];
            }

            a[j] = temp;
        }

        System.out.println(count + a.length);
    }
    public static void bubbleSort2(int[] a){
        int count = 0;
        boolean swapped = true;

        while(swapped){
            swapped = false;
            for (int i = 0; i < a.length - 1; i++){
                if (a[i] > a[i+1]){
                    swap(a, i, i+1);
                    swapped = true;
                    count++;
                }
            }
            System.out.println(Arrays.toString(a));

        }

        System.out.println(count);
    }
    public static void bogoSort(int[] a){
        int count = 0;
        while (!isSorted(a)){
            shuffle(a);
            count++;
        }

        System.out.println(count);
    }

    public static boolean isSorted(int[] a){
        for (int i = 0; i < a.length - 1; i++){
            if (a[i] > a[i + 1])
                return false;
        }

        return true;
    }

    public static void shuffle(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            // pick random number in [i+1, a.length-1] inclusive
            int range = (a.length - 1) - (i + 1) + 1; //(a.length – 1) - (i + 1) + 1;
            int j = (int)(Math.random() * range + (i + 1));
            swap(a, i, j);
        }
    }

    // Swaps a[i] with a[j].
    private static void swap(int[] a, int i, int j) {
        if (i == j)
            return;

        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }


}
